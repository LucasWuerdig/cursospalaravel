<?php

namespace App\ActiveRecord;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
        'post_id',
        'text',
        'data'
    ];

    public function user()
    {
        return $this->belongsTo('App\ActiveRecord\User');
    }

    public function post()
    {
        return $this->belongsTo('App\ActiveRecord\Post');
    }
}
