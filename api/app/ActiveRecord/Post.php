<?php

namespace App\ActiveRecord;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title',
        'text',
        'image',
        'link',
        'data'
    ];

    public function comments()
    {
        return $this->hasMany('App\ActiveRecord\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\ActiveRecord\User');
    }

    public function likes()
    {
        return $this->belongsToMany('App\ActiveRecord\User', 'likes','post_id','user_id');
    }
}
