<?php

namespace App\ActiveRecord;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'imagem'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany('App\ActiveRecord\Comment');
    }

    public function posts()
    {
        return $this->hasMany('App\ActiveRecord\Post');
    }

    public function likes()
    {
        return $this->belongsToMany('App\ActiveRecord\Post', 'likes','user_id','post_id');
    }

    public function friends()
    {
        return $this->belongsToMany('App\ActiveRecord\User', 'friends','user_id','friend_id');
    }
}
