<?php

namespace App\Http\Controllers;

use App\Services\LoginServiceContract;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    protected $loginService;

    public function __construct(LoginServiceContract $contract)
    {
        $this->loginService = $contract;
    }

    public function authenticate(Request $request)
    {
        return $this->loginService->authenticate($request->only('email', 'password'));
    }
}
