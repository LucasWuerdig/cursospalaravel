<?php

namespace App\Providers;

use App\Repositories\Contracts\UserContract;
use App\Repositories\UserRepository;
use App\Services\LoginService;
use App\Services\LoginServiceContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserContract::class, UserRepository::class);
        $this->app->bind(LoginServiceContract::class, LoginService::class);
    }
}
