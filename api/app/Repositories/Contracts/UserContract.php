<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 19/10/18
 * Time: 09:28
 */

namespace App\Repositories\Contracts;


use App\ActiveRecord\User;

interface UserContract
{
    public function __construct(User $repository);

    public function create($request);

    public function update($request, $id);
}