<?php
namespace App\Repositories;

use App\ActiveRecord\User;
use App\Repositories\Contracts\UserContract;
use App\Traits\ResponseJsonTrait;
use Exception;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserRepository implements UserContract
{
    use ResponseJsonTrait;

    protected $entity;

    public function __construct(User $repository)
    {
        $this->entity = $repository;
    }

    public function create($request)
    {
        $validation = $this->createUserRequest($request);

        if ($validation->fails()) {
            return $this->validatioError($validation->errors(), 'Falha ao validar cadastro' ,400);
        }

        try {
            $user = User::create([
                'name'     => $request->input('name'),
                'email'    => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);

            $user->token = $user->createToken($user->email)->accessToken;

            return $this->responseSuccess($user);
        } catch (Exception $e) {
            return $this->responseError($e);
        }
    }

    public function update($request, $id) {
        $data = $request->all();
        $validation = $this->updateUserRequest($request, $id);

        if ($validation->fails()) {
            return $this->validatioError($validation->errors(), 'Falha ao validar cadastro' ,400);
        }

        if (isset($data['password']) && isset($data['password_confirmation'])) {
            $data['password'] = bcrypt($data['password']);
        }

        if(isset($data['imagem']) && !is_null($data['imagem'])) {
            $user = $this->entity->find($id);
            if (asset($user->imagem) != $data['imagem']) {
                if(file_exists($user->imagem)) {
                    unlink($user->imagem);
                }
                $urlImagem = $this->uploadImagem($data['imagem'], $id);
                $data['imagem'] = $urlImagem;
            }
        }

        try {
            $user = $this->entity->find($id);
            $user->update($data);
            $user->imagem = asset($user->imagem);

            return $this->responseSuccess($user);

        } catch (Exception $exception) {
            return $this->responseError($exception, 'Erro ao atualizar usuario');

        }
    }

    protected function createUserRequest($request) {

        $validation = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        return $validation;
    }

    protected function updateUserRequest($request, $id) {

        $validation = Validator::make($request->all(),[
            'name'     => 'required|string|max:255',
            'email'    => ['required','string','email','max:255', Rule::unique('users')->ignore($id)],
            'password' => 'sometimes|nullable|string|min:6|confirmed',
        ]);

        return $validation;
    }

    protected function uploadImagem($image64, $userId) {
        $time = time();
        $baseDirectory = 'perfils';
        $ImgDirectory  = $baseDirectory.DIRECTORY_SEPARATOR.'perfil_id'.$userId;
        $ext = substr($image64, 11, strpos($image64, ';') - 11);
        $urlImagem = $ImgDirectory.DIRECTORY_SEPARATOR.$time.".".$ext;

        $file = str_replace('data:image/'.$ext.";base64,", '', $image64);
        $file = base64_decode($file);

        if (!file_exists($baseDirectory)) {
            mkdir($baseDirectory, 0700);
        }
        if (!file_exists($ImgDirectory)) {
            mkdir($ImgDirectory, 0700);
        }

        file_put_contents($urlImagem, $file);

        return $urlImagem;
    }

}