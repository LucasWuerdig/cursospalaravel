<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 19/10/18
 * Time: 15:03
 */

namespace App\Services;

use Exception;
use App\Traits\ResponseJsonTrait;
use Illuminate\Support\Facades\Validator;


class LoginService implements LoginServiceContract
{
    use ResponseJsonTrait;

    public function authenticate(array $credentials)
    {
        $validation = $this->LoginRequest($credentials);
        if ($validation->fails()) {
            return $this->validatioError($validation->errors(), 'Erro form',400);
        }

        try {
            if (! $user = auth()->attempt($credentials)) {
//                throw new Exception('credenciais inválidas',401);
                return $this->validatioError([ 'msg' => [ 'Credenciais Inválidas' ] ],401);
            }
            $user = auth()->user();
            $user->imagem = asset($user->imagem);
            $user->token = $user->createToken($user->email)->accessToken;

            return $this->responseSuccess($user);
        } catch (Exception $exception) {
            return $this->responseError($exception);
        }
    }

    protected function LoginRequest($request) {

        $validation = Validator::make($request,[
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);

        return $validation;
    }
}