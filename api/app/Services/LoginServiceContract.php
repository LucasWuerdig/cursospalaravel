<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 19/10/18
 * Time: 15:05
 */

namespace App\Services;


interface LoginServiceContract
{
    public function authenticate(array $credentials);
}