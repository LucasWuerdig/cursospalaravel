<?php
/**
 * Created by PhpStorm.
 * User: lucas
 * Date: 18/10/18
 * Time: 16:14
 */

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait ResponseJsonTrait
{
    public function responseSuccess($response, string $message = 'Success Operation', int $statusCode = 200, array $customHeader = []) : JsonResponse {
        $response = [
            'status_code' => $statusCode,
            'message'     => $message,
            'result'      => $response
        ];

        return response()->json($response, $statusCode, $customHeader);
    }

    public function responseError($exception, string $message = null, int $statusCode = 400) : JsonResponse {
        $statusCode = $exception->getCode() === 0 ? $statusCode : $exception->getCode();

        $response = [
            'status_code' => $statusCode,
            'message'     => !$message ? $exception->getMessage() : $message,
            'result'      => $exception->getMessage()
        ];

        return response()->json($response, $statusCode);
    }

    public function validatioError($errors, string $message = null, int $statusCode = 400) : JsonResponse {
        $response = [
            'status_code' => $statusCode,
            'message'     => $message,
            'result'      => $errors
        ];

        return response()->json($response, $statusCode);
    }
}