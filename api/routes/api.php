<?php

use App\ActiveRecord\User;
use App\ActiveRecord\Post;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('authenticate', 'LoginController@authenticate');
Route::post('userStore', 'UserController@store');
Route::resource('user', 'UserController', ['middleware' => 'auth:api', 'only' => ['update'] ]);

/*Route::middleware('auth:api', function () {
    Route::resource('user', 'UserController', ['except' => ['store'] ]);
});*/

//Route::middleware('auth:api')->get('/usuario', function (Request $request) {
//    return $request->user();
//});