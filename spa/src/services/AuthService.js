import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: `${process.env.VUE_APP_API_ENDPOINT}`
})

const storeToken = token => {
    axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
    sessionStorage.setItem('token', token)

    return Promise.resolve(true)
}

const clearToken = () => {
    axios.defaults.headers.common['Authorization'] = null
}

const authenticate = credentials => {
    return axiosInstance.post('authenticate', credentials)
        .then(({data}) => data)
}

export default {
    authenticate,
    storeToken,
    clearToken
}