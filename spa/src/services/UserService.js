import axios from 'axios'

const axiosInstance = axios.create({
    baseURL: `${process.env.VUE_APP_API_ENDPOINT}`
})

const create = form => {
    return axiosInstance.post('userStore', form)
        .then(({data}) => data)
}

const update = form => {
    return axiosInstance.put('user/' + form.id, form)
        .then(({data}) => data)
}

export default {
    create,
    update
}