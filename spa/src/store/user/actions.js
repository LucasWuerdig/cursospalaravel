import AuthService from '@/services/AuthService'
import UserService from '@/services/UserService'

export const authenticate = (context, credentials) => {
  return new Promise((resolve, reject) => {
    AuthService.authenticate(credentials)
        .then(response => {
            context.commit('SET_USER', response.result)
            sessionStorage.setItem('user', JSON.stringify(response.result))
            AuthService.storeToken(response.result.token)

            resolve(response)
        })
        .catch(error => {

          let dados   = error.response.data

          reject(dados.result)
        })
  })
}

export const logout = (context) => {
    return new Promise((resolve) => {
        AuthService.clearToken()
        sessionStorage.clear()
        context.commit('SET_USER', false)

        resolve(true)
    })
}

export const create = (context, form) => {
    return new Promise((resolve, reject) => {
        UserService.create(form)
            .then(response => {
                context.commit('SET_USER', response.result)
                sessionStorage.setItem('user', JSON.stringify(response.result))
                AuthService.storeToken(response.result.token)

                resolve(response)
            })
            .catch(error => {
                let dados = error.response.data

                reject(dados.result)
            })
    })
}

export const update = (context, form) => {
    return new Promise((resolve, reject) => {
        UserService.update(form)
            .then(response => {
                context.commit('SET_USER', response.result)
                sessionStorage.setItem('user', JSON.stringify(response.result))

                resolve(true)
            })
            .catch(error => {
                let dados = error.response.data

                reject(dados.result)
            })
    })
}